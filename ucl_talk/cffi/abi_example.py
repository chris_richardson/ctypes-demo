from cffi import FFI
ffi = FFI()
ffi.cdef("void add(double *in_data, double *out_data, int n);")
mylib = ffi.dlopen("./mylib.so")

import numpy as np
x = np.array([1, 2, 3], dtype=np.float64)
y = np.array([10, 11, 12], dtype=np.float64)

mylib.add(ffi.cast("double *", x.ctypes.data),
          ffi.cast("double *", y.ctypes.data), 3)

print(x, y)
