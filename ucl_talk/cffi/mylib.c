
#include "math.h"

void add(double *in_data, double *out_data, int n)
{
  int i;

  for (i = 0; i != n; ++i)
      out_data[i] += in_data[i];
}
