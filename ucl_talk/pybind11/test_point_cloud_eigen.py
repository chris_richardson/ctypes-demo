
from point_cloud import PointCloud
import numpy

n = 5
pts = numpy.random.random((n, 3))

p = PointCloud(pts)  # numpy array -> Eigen::MatrixXd
d = p.calculate_distances()  # Eigen::MatrixXd -> numpy array

print(d)
