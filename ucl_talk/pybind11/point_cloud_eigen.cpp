#include <Eigen/Dense>
#include <cmath>
#include <vector>

class PointCloud {
public:
  PointCloud(Eigen::MatrixXd points) : _points(points) {}

  Eigen::MatrixXd calculate_distances() {
    unsigned int n = _points.rows();
    Eigen::MatrixXd result(n, n);

    for (unsigned int i = 0; i != n; ++i)
      for (unsigned int j = 0; j != n; ++j)
        result(i, j) = distance(i, j);

    return result;
  }

private:
  double distance(unsigned int i, unsigned int j) {
    return (_points.row(i) - _points.row(j)).norm();
  }

  Eigen::MatrixXd _points;
};

#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

// Wrappers using pybind11
namespace py = pybind11;
PYBIND11_MODULE(point_cloud, m) {
  py::class_<PointCloud>(m, "PointCloud")
      .def(py::init<Eigen::MatrixXd>())
      .def("calculate_distances", &PointCloud::calculate_distances);
}
