
from point_cloud import PointCloud
import numpy

n = 5
pts = numpy.random.random((n, 3))

p = PointCloud(list(pts.flatten()))  # Python list -> std::vector
d_list = p.calculate_distances()  # Returns std::vector -> list
d = numpy.array(d_list).reshape((n, n))  # Reshape with numpy

print(d)
