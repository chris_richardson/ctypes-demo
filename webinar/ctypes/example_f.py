
from ctypes import CDLL, POINTER, c_double, c_int, byref
import numpy

x = numpy.array([0.0, 1.0, 2.0], dtype=numpy.float64)
y = numpy.zeros_like(x)

mylib = CDLL("./fort.so")

n = c_int(len(x))
mylib.add_(x.ctypes.data_as(POINTER(c_double)), y.ctypes.data_as(POINTER(c_double)), byref(n))
print(y)

mylib.add_(x.ctypes.data_as(POINTER(c_double)), y.ctypes.data_as(POINTER(c_double)), byref(n))
print(y)
