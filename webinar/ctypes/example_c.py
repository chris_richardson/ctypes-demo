
from ctypes import CDLL, POINTER, c_double
import numpy

x = numpy.array([0.0, 1.0, 2.0], dtype=numpy.float64)
y = numpy.zeros_like(x)

mylib = CDLL("./mylib.so")

mylib.add(x.ctypes.data_as(POINTER(c_double)), y.ctypes.data_as(POINTER(c_double)), 3)
print(y)

mylib.add(x.ctypes.data_as(POINTER(c_double)), y.ctypes.data_as(POINTER(c_double)), 3)
print(y)
