# Example demonstrating numba cfunc to provide a compiled callback kernel

from ctypes import POINTER, c_double, c_int
from numba import cfunc, types, carray, jit
import numpy as np


# This is the kernel we want to call from our bigger library
def calculate_distances(_d, _pts, n):
    d = carray(_d, (n, n), dtype=np.float64)
    pts = carray(_pts, (n, 3), dtype=np.float64)

    for i in range(n):
        for j in range(n):
            d[i, j] = np.linalg.norm(pts[i, :] - pts[j, :])

sig = types.void(types.CPointer(types.double), types.CPointer(types.double), types.intc)  # C signature
fn = cfunc(sig, nopython=True)(calculate_distances)  # compile with LLVM

# Take a look at the code
print(fn.inspect_llvm())

# Show the memory address of the function
print('0x%0x'%fn.address)

n = 12
d = np.zeros((n, n), dtype=np.float64)
pts = np.random.random((n, 3))

# Convenient ctypes wrapper allows us to test it from Python
fn.ctypes(d.ctypes.data_as(POINTER(c_double)),
          pts.ctypes.data_as(POINTER(c_double)), n)

np.set_printoptions(precision=2)
print(d)
