from numba import jit
import numpy

@jit
def calculate_distances(n):
    pts = numpy.random.random((n, 3))
    d = numpy.zeros((n, n))

    for i in range(n):
        for j in range(n):
            d[i, j] = numpy.linalg.norm(pts[i,:] - pts[j,:])
    return d

n = 1000
d = calculate_distances(n)

llvm = calculate_distances.inspect_llvm()
for k, v in llvm.items():
    print(k, v)

print(d)
