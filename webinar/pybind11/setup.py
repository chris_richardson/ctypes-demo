from distutils.core import setup
from distutils.extension import Extension

import pybind11
pybind11_include = pybind11.get_include()

setup(name='point_cloud',
      version='1.0',
      ext_modules=[Extension("point_cloud",
                             ['point_cloud.cpp'],
                             language='c++',
                             include_dirs=[pybind11_include, '/usr/include/eigen3'],
                             extra_compile_args=['-std=c++11'])])
