# README #

Some examples using ctypes and pybind11

### Compiling

There is a script "build.sh" to compile the code. 
The ctypes examples should build easily.
However, it will need some editing for the pybind11 example, to find the headers and libraries.

