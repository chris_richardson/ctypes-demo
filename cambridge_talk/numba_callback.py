
from ctypes import CDLL, POINTER, c_double, CFUNCTYPE

c_point_cloud = CDLL("./c_point_cloud_callback.so")

from numba import cfunc, types, carray
import numpy as np

def distance(x_, y_):
    # Convert input arguments from pointers to numpy arrays
    x = carray(x_, (3), dtype=np.float64)
    y = carray(y_, (3), dtype=np.float64)
    d = x - y
    return np.sqrt(d[0]**2 + d[1]**2 + d[2]**2)

sig = types.double(types.CPointer(types.double), types.CPointer(types.double))
fn = cfunc(sig)(distance)

n = 1000
pts = np.random.random((n, 3))
d = np.zeros((n, n), dtype=np.float64)

# Use the JIT compiled distance routine above
dist_fn = fn.ctypes
# Uncomment this to use the "distance" routine in the C library
# dist_fn = c_point_cloud.distance

c_point_cloud.compute_array(pts.ctypes.data_as(POINTER(c_double)),
                            d.ctypes.data_as(POINTER(c_double)), n, dist_fn)

print(d[-2])
