
#include "stdio.h"
#include "math.h"

double distance(double *p1, double *p2)
{
  double d0 = (p1[0] - p2[0]);
  double d1 = (p1[1] - p2[1]);
  double d2 = (p1[2] - p2[2]);
  return sqrt(d0*d0 + d1*d1 + d2*d2);
}


void compute_array(double *pts, double *result, int n)
{
  int i, j;

  printf("%d\n", n);

  for (i = 0; i < n; ++i)
  {
    for (j = 0; j < i; ++j)
    {
      double d = distance(pts + i*3, pts + j*3);
      result[i*n + j] = d;
    }
  }
}
