
from cpp_point_cloud import point_cloud
import numpy

n = 10
pts = numpy.random.random((n, 3))

p = point_cloud(list(pts.flatten()))
d = numpy.array(p.calculate_distances()).reshape((n, n))

print(d)
