
import numpy

def distance(r1, r2):
    x = r2 - r1
    return numpy.linalg.norm(x)

n = 1000
pts = numpy.random.random((n, 3))
d = numpy.zeros((n, n))

for i in range(n):
    for j in range(i):
        d[i, j] = distance(pts[i,:], pts[j,:])

print(d[-1])
