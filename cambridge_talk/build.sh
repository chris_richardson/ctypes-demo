# Compile libraries

gcc -shared -fPIC -o mylib.so mylib.c
gfortran -shared -fPIC -o fort.so fort.f
gcc -shared -fPIC -o c_point_cloud.so c_point_cloud.c
gcc -shared -fPIC -o c_point_cloud_callback.so c_point_cloud_callback.c

g++ -std=c++11 -shared -o cpp_point_cloud.so cpp_point_cloud.cpp -lm -I/usr/local/include/python3.6m -I /usr/local/Cellar/python3/3.6.4/Frameworks/Python.framework/Versions/3.6/include/python3.6m -L /usr/local/Cellar/python3/3.6.4/Frameworks/Python.framework/Versions/3.6/lib -lpython3.6
