
#include <cmath>
#include <vector>

class point_cloud {
public:
  point_cloud(std::vector<double> &points) : _points(points) {
    if (_points.size() % 3 != 0)
      throw std::runtime_error("Wrong vector size");
  }

  std::vector<double> calculate_distances() {

    unsigned int n = _points.size() / 3;
    std::vector<double> result(n * n);

    for (unsigned int i = 0; i != n; ++i)
      for (unsigned int j = 0; j != i; ++j)
        result[i * n + j] = distance(i, j);

    return result;
  }

private:
  double distance(unsigned int i, unsigned int j) {
    double d = 0.0;
    for (unsigned int k = 0; k != 3; ++k) {
      double d0 = _points[i * 3 + k] - _points[j * 3 + k];
      d += d0 * d0;
    }
    return std::sqrt(d);
  }

  std::vector<double> _points;
};

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

// Wrappers using pybind11
namespace py = pybind11;
PYBIND11_MODULE(cpp_point_cloud, m) {
  py::class_<point_cloud>(m, "point_cloud")
      .def(py::init<std::vector<double> &>())
      .def("calculate_distances", &point_cloud::calculate_distances);
}
