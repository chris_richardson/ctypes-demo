from ctypes import CDLL, POINTER, c_double
import numpy

c_point_cloud = CDLL("./c_point_cloud.so")

n = 1000
pts = numpy.random.random((n, 3))
d = numpy.zeros((n, n), dtype=numpy.float64)
c_point_cloud.compute_array(pts.ctypes.data_as(POINTER(c_double)),
                            d.ctypes.data_as(POINTER(c_double)), n)

print(d[-1])
